﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Danger : MonoBehaviour {

    public GameObject RollerBall;

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.name == "Danger")
        {
            //Load scene from beginning
            //SceneManager.LoadScene("scene");
            RollerBall = Instantiate(Resources.Load("RollerBall")) as GameObject;
        }

        Destroy(gameObject);
    }

}
