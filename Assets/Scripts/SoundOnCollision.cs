﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOnCollision : MonoBehaviour {

    public AudioClip sound;         // Variable for sound to be set in inspector

    AudioSource myAudio;            // Variable for storing the reference to AudioSource

    void Start() {
        myAudio = GetComponent<AudioSource>(); // Find a reference to the AudioSource 
    }                                         // that is attached to this gameobject

    void OnCollisionEnter() {
        myAudio.PlayOneShot(sound);
    }
}
